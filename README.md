# formative15

**Branches after merging Branch1 & Branch2 with TestingBranches**

![branches](./images/branches.PNG)

**Dev2's Git terminal screen after pushing to online repository**

![branches](./images/conflict1.PNG)

**Dev2's Git terminal screen after pulling to testing-conflict branch**

![branches](./images/conflict2.PNG)

**Dev2's text file after getting the conflict**

![branches](./images/conflict3.PNG)
